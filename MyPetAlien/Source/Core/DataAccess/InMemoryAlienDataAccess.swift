//
//  InMemoryAlienDataAccess.swift
//  MyPetAlien
//
//  Created by Josh Berlin on 5/19/15.
//  Copyright (c) 2015 CocoaConf. All rights reserved.
//

import Foundation

class InMemoryAlienDataAccess: AlienDataAccess {
  
  let aliens: [Alien]
  
  init(aliens: [Alien]) {
    self.aliens = aliens
  }
  
  func getAliens() -> [Alien] {
    return aliens
  }
  
  func getAlienForID(alienID: String) -> Alien? {
    return aliens.filter({ $0.id == alienID }).first
  }
  
}
