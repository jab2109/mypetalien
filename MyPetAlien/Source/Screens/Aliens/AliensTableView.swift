//
//  AliensTableView.swift
//  Tailio
//
//  Created by Josh Berlin on 4/1/15.
//  Copyright (c) 2015 Pet Wireless. All rights reserved.
//

import UIKit

protocol AliensUserInterface: class {
  func setInteractionHandler(interactionHandler: AliensInteractionHandler)
  func setDisplayableAliens(displayableAliens: [DisplayableAlien])
}

let AlienCellIdentifier = "AlienCellIdentifier"
let AliensHeaderIdentifier = "AliensHeaderIdentifier"

class AliensTableView: UITableView {
  
  weak var interactionHandler: AliensInteractionHandler?
  
  let aliensHeaderIconImage = UIImage(named: "AlienIcon")!
  
  var displayableAliens: [DisplayableAlien] = [DisplayableAlien]() {
    didSet {
      reloadData()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    backgroundColor = UIColor.greyBackgroundColor()
    separatorColor = UIColor.cellSeparatorColor()
    delegate = self
    dataSource = self
  }
  
}

extension AliensTableView: AliensUserInterface {
  
  func setInteractionHandler(interactionHandler: AliensInteractionHandler) {
    self.interactionHandler = interactionHandler
  }
  
  func setDisplayableAliens(displayableAliens: [DisplayableAlien]) {
    self.displayableAliens = displayableAliens
  }
  
}

// MARK: Table view data source

extension AliensTableView: UITableViewDataSource {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return displayableAliens.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(AlienCellIdentifier, forIndexPath: indexPath) as! AlienCell
    cell.accessoryType = .DisclosureIndicator
    let displayableAlien = displayableAliens[indexPath.row]
    cell.setDisplayableAlien(displayableAlien)
    return cell
  }
  
}

// MARK: Table view delegate

extension AliensTableView: UITableViewDelegate {
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 150.0
  }
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40.0
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = AliensHeaderView(frame: CGRectZero)
    headerView.backgroundColor = UIColor.clearColor()
    headerView.headerLabel.text = "Aliens"
    headerView.iconImageView.image = aliensHeaderIconImage
    return headerView
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    let displayableAlien = displayableAliens[indexPath.row]
    interactionHandler?.showAlienDetailForAlien(displayableAlien.id)
  }
  
}
