//
//  AliensNavigationController.swift
//  MyPetAlien
//
//  Created by Josh Berlin on 5/19/15.
//  Copyright (c) 2015 CocoaConf. All rights reserved.
//

import UIKit

class AliensNavigationController: UINavigationController {
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return .LightContent
  }
  
}
